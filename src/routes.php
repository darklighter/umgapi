<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response) {
    // Render index view
    return $this->renderer->render($response, 'index.phtml');
});

$app->get('/actor/{id}', function (Request $request, Response $response, array $args) {

    $actor_id = (int)$args['id'];

    $db = $this->get('db');

    $results = $db->query('SELECT * FROM `actor` WHERE actor_id = ' . $actor_id, PDO::FETCH_ASSOC );

    return $response->withJson( ( $results ) ? $results->fetchAll() : [] );

});

$app->get('/actors/{id}', function (Request $request, Response $response, array $args) {

    $movie_id = (int)$args['id'];

    $db = $this->get('db');

    $results = $db->query(
      'SELECT ' .
        'actor.actor_id, ' .
        'CONCAT(UCASE(SUBSTR(actor.first_name,1,1)), ' .
          'LCASE(SUBSTR(actor.first_name,2,LENGTH(actor.first_name))), ' .
        "_utf8' ', " .
        'CONCAT(UCASE(SUBSTR(actor.last_name,1,1)), ' .
          'LCASE(SUBSTR(actor.last_name,2,LENGTH(actor.last_name))))) as name ' .
      'FROM actor ' .
        'LEFT JOIN film_actor ON actor.actor_id = film_actor.actor_id ' .
      'WHERE film_actor.film_id = ' . $movie_id . ' ORDER BY actor.last_name', PDO::FETCH_ASSOC );

    return $response->withJson( ( $results ) ? $results->fetchAll() : [] );

});

$app->get('/categories', function (Request $request, Response $response, array $args) {

    $db = $this->get('db');

    $results = $db->query('SELECT category_id, name FROM `category`', PDO::FETCH_ASSOC );

    return $response->withJson( $results->fetchAll() );

});

$app->get('/movie/{id}', function (Request $request, Response $response, array $args) {

    $film_id = (int)$args['id'];

    $db = $this->get('db');

//    far simpler ... and more boring.
//     $results = $db->query('SELECT * FROM `film` WHERE film_id = ' . $film_id, PDO::FETCH_ASSOC );

    $results = $db->query(
      'SELECT ' .
        'film.film_id AS FID, film.title AS title, film.description AS description, category.name AS category, ' .
        'film.rental_rate AS price, ' .
        'film.special_features, language.name AS language, ' .
        'film.length AS length, film.rating AS rating, GROUP_CONCAT(CONCAT(CONCAT(UCASE(SUBSTR(actor.first_name,1,1)), ' .
        'LCASE(SUBSTR(actor.first_name,2,LENGTH(actor.first_name))),_utf8\' \',CONCAT(UCASE(SUBSTR(actor.last_name,1,1)), ' .
        'LCASE(SUBSTR(actor.last_name,2,LENGTH(actor.last_name)))))) SEPARATOR \', \') AS actors ' .
      'FROM category ' .
        'LEFT JOIN film_category ON category.category_id = film_category.category_id ' .
        'LEFT JOIN film ON film_category.film_id = film.film_id ' .
        'LEFT JOIN language ON film.language_id = language.language_id ' .
        'JOIN film_actor ON film.film_id = film_actor.film_id ' .
        'JOIN actor ON film_actor.actor_id = actor.actor_id ' .
      'WHERE film.film_id = ' . $film_id . ' ' .
      'GROUP BY film.film_id, category.name', PDO::FETCH_ASSOC );

    return $response->withJson( ( $results ) ? $results->fetchAll() : [] );

});

$app->get('/ratings', function (Request $request, Response $response, array $args) {

    return $response->withJson([ 'G','PG','PG-13','R','NC-17' ]);

});

$app->get('/search', function (Request $request, Response $response, array $args) {

   $prms = $request->getQueryParams();

   if ( !$prms[ 'title' ] ) { return $response->withJson([]); }

   $rtng = ( !$prms[ 'rating' ] ) ? '' : ' AND (rating LIKE "' . $prms[ 'rating' ] . '") ';

   if ( !$prms[ 'category' ] ) {
     $ctgry = $cgjoin = '';
   }
   else {
     $cgjoin = 'LEFT JOIN film_category ON film_category.film_id = film.film_id ' .
       'LEFT JOIN category ON category.category_id = film_category.category_id ' .

     $ctgry = ' AND ((category.name LIKE "%' . $prms[ 'category' ] . '%") OR (category.category_id = ' . $prms[ 'category' ] . ')) ';
   }

   $fields = ( $prms[ 'details' ] == 'all') ? 'film.*' : ' film.film_id, film.title, film.description ';

   $db = $this->get('db');

   $results = $db->query(
     'SELECT ' . $fields .
     'FROM `film` ' .
       $cgjoin .
     'WHERE title LIKE "%' . $prms[ 'title' ] . '%" ' .
     $rtng . $ctgry .
     'LIMIT 50', PDO::FETCH_ASSOC );

   return $response->withJson( ( $results ) ? $results->fetchAll() : [] );

});