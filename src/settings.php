<?php
return [
  'settings' => [
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
    'displayErrorDetails' => true, // set to false in production

    //  database settings
    'db' => [
      'name' => 'sakila',
      'host' => 'localhost',
      'password' => 'sakila',
      'username' => 'sakila'
    ],

    // Monolog settings
    'logger' => [
      'level' => \Monolog\Logger::DEBUG,
      'name' => 'slim-app',
      'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log'
    ],

    // Renderer settings
    'renderer' => [
      'template_path' => __DIR__ . '/../templates/'
    ]
  ]
];
