<?php
// DIC configuration
$container = $app->getContainer();

$container['db'] = function ($c) {
  $db = $c['settings']['db'];

  return new PDO("mysql:host={$db['host']};dbname={$db['name']};charset=utf8", $db['username'], $db['password']);
};

// monolog
$container['logger'] = function ($c) {
  $settings = $c->get('settings')['logger'];
  $logger = new Monolog\Logger($settings['name']);
  $logger->pushProcessor(new Monolog\Processor\UidProcessor());
  $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
  return $logger;
};

// view renderer
$container['renderer'] = function ($c) {
  $settings = $c->get('settings')['renderer'];
  return new Slim\Views\PhpRenderer($settings['template_path']);
};