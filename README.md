# UMG·api - a small API for the Ultimate Moviegoer's Guide.

UMG·api is a piece of demonstration software.

There is an online instance of this at [umgapi.ginomai.com][lnk_umg].


[lnk_umg]: https://umgapi.ginomai.com
